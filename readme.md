# ELK-WINDOWS

For installing ELK on locally on windows

### Prerequisities

Java JRE at least 1.7

Ensure that java is on your path and the JAVA_HOME environment variable is set

Powershell 3 
```
https://www.microsoft.com/en-gb/download/details.aspx?id=34595
```

### Installing

run this script

```
scripts/install.ps1
```