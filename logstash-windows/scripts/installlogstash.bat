@echo off


IF "%~1"=="" goto setDefaultEnvironment
IF NOT "%~1"=="" goto setEnvironmentFromCommandLine


:setDefaultEnvironment
SET LOGSTASH_ENV=dev
goto transformConfigs

:setEnvironmentFromCommandLine
SET LOGSTASH_ENV="%~1"
goto transformConfigs


:transformConfigs

powershell -command ".\installlogstash.ps1 -env %LOGSTASH_ENV%; exit $LASTEXITCODE"

