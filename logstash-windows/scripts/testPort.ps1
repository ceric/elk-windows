function Test-Port($hostname, $port, $attempts)
{

     for($i=1; $i -le $attempts; $i++)
    {
        Write-Host "Checking $hostname on port $port attempt no: $i"
        If ((Try-Port $hostname $port) -eq $true) {
            break;
        }
        Start-Sleep -s 1
    }

}

function Try-Port($hostname, $port)
{

    $open = $false
    # This works no matter in which form we get $host - hostname or ip address
    try {
        $ip = [System.Net.Dns]::GetHostAddresses($hostname) | 
            select-object IPAddressToString -expandproperty  IPAddressToString
        if($ip.GetType().Name -eq "Object[]")
        {
            #If we have several ip's for that address, let's take first one
            $ip = $ip[0]
        }
    } catch {
        Write-Host "Possibly $hostname is wrong hostname or IP"
        return
    }
    $t = New-Object Net.Sockets.TcpClient
    # We use Try\Catch to remove exception info from console if we can't connect
    try
    {
        $t.Connect($ip,$port)
    } catch {}

    if($t.Connected)
    {
        $t.Close()
        $open = $true
        $msg = "Port $port is open"
    }
    else
    {
        $msg = "Port $port on $ip is closed, "
    }
    Write-Host $msg
    return $open
}