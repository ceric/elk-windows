@echo off


IF "%~1"=="" goto setDefaultEnvironment
IF NOT "%~1"=="" goto setEnvironmentFromCommandLine


:setDefaultEnvironment
SET CURATOR_ENV=dev
goto transformConfigs

:setEnvironmentFromCommandLine
SET CURATOR_ENV="%~1"
goto transformConfigs


:transformConfigs

powershell -command ".\installcurator.ps1 -env %CURATOR_ENV%; exit $LASTEXITCODE"

