param ([string]$env)


. .\testPort.ps1
. .\nssmInstallService.ps1
. .\unzip.ps1


$environments = @{
        dev = @{
            forwardTo = '["127.0.0.1:9200"]';
            }; 
        qa = @{
            forwardTo = '["*****:9200"]';
            };
        live = @{
            forwardTo = '["*******:9200"]';
            };
    }



function AddFirewallRule($name, $port)
{
    $check = iex "netsh advfirewall firewall show rule name=`"$name`""
    Write-Host $check

    if(-Not($check -like "Rule*")) 
    {
        Write-Host "Adding firewall rule `"$name`" to open port $port"
        iex "netsh advfirewall firewall add rule name=`"$name`" dir=in action=allow protocol=TCP localport=$port"
    }
}

function Unpack {
    param([string]$zipFile, [string]$target)

      If (-Not (Test-Path $target)){
        Unzip $zipFile $target
    }
}

function InstallLogstash {

    param([string]$env)

    $forwardTo = $environments.$env.forwardTo 
     Write-Host "$env environment forwarding logs to $forwardTo" -foregroundcolor White -backgroundcolor DarkRed


    #unpack nssm and logstash
    $logstashDeploymentFolder = "$PSScriptRoot\..\logstash-2.4.0"
    Unpack "$PSScriptRoot\..\logstash-2.4.0.zip" $logstashDeploymentFolder

    $nssmUnzipTo = "$PSScriptRoot\..\nssm"
    Unpack "$PSScriptRoot\..\nssm-2.24.zip" $nssmUnzipTo


    #stop service and delete
    iex "net stop logstash"
    Uninstall-Service logstash

    AddFirewallRule "open port 5044" 5044

    #copy start script for service
    Copy-Item "$PSScriptRoot\runlogstash.bat" "$logstashDeploymentFolder\logstash-2.4.0\bin" -force

    #copy logstash config to bin
    (Get-Content "$PSScriptRoot\logstash.conf").replace('${forwardTo}',  $forwardTo ) | Set-Content "$logstashDeploymentFolder\logstash-2.4.0\bin\logstash.conf" -force
    
    iex "cmd.exe /C `"$logstashDeploymentFolder\logstash-2.4.0\bin\logstash-plugin install logstash-filter-de_dot`""

    #install service
    Install-Service "logstash" "$logstashDeploymentFolder\logstash-2.4.0\bin\runlogstash.bat"    
    iex 'net start logstash'

    #check logstash port
    Test-port 127.0.0.1 5044 10
}

InstallLogstash($env)