param ([string]$env)

. .\unzip.ps1


$environments = @{
        dev = @{
            elasticSearchIp = '["127.0.0.1:9200"]';
            }; 
        qa = @{
            elasticSearchIp = '["*************:9200"]';
            };
        live = @{
            elasticSearchIp = '["**************:9200"]';
            };
    }


function Unpack {
    param([string]$zipFile, [string]$target)

      If (-Not (Test-Path $target)){
        Unzip $zipFile $target
    }
}

function InstallCurator {

    param([string]$env)

    $elasticSearchIp = $environments.$env.elasticSearchIp 
     Write-Host "$env environment curating elastic search at $elasticSearchIp" -foregroundcolor White -backgroundcolor DarkRed


    #unzip curator
    $curatorDeploymentFolder = "$PSScriptRoot\..\curator-4.2.4-win32"
    Unpack "$PSScriptRoot\..\curator-4.2.4-win32.zip" $curatorDeploymentFolder


    #copy configs to unzipped folder
    Copy-Item "$PSScriptRoot\..\curatorConfig" "$curatorDeploymentFolder\" -force

    #replace es placeholder
    (Get-Content "$PSScriptRoot\..\curatorConfig\curator.yml").replace('${elasticSearchIp}',  $elasticSearchIp ) | Set-Content "$curatorDeploymentFolder\curatorConfig\curator.yml" -force
    
}

InstallCurator($env)