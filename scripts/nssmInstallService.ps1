$pathToNssm = "$PSScriptRoot\..\nssm\nssm-2.24\win64\nssm.exe"

function Uninstall-Service
{
     param([string]$serviceName)

     iex "$pathToNssm remove $serviceName confirm"
}

function Install-Service
{
    param([string]$serviceName, [string]$path)

    Uninstall-Service $serviceName

    Write-Host "CREATING SERVICE $serviceName AT $path"

    iex "$pathToNssm install $serviceName $path"

}