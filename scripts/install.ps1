. .\unzip.ps1
. .\testPort.ps1
. .\nssmInstallService.ps1

function Unpack
{
    param([string]$location, [string]$zipFile, [string]$unzipToFolder)

    $downloadedToPath = "$PSScriptRoot\..\$zipFile"
    $unzippedToPath = "$PSScriptRoot\..\$unzipToFolder"

    If (-Not (Test-Path $downloadedToPath)){
        Invoke-WebRequest $location -OutFile $downloadedToPath
    }

    If (-Not (Test-Path $unzippedToPath)){
        Unzip $downloadedToPath $unzippedToPath
    }
}


function StartAll
{
    iex 'net start elasticsearch-service-x64'
    iex 'net start logstash'
    iex 'net start kibana'
    iex 'net start filebeat'
}

function AddFirewallRule($name, $port)
{
    $check = iex "netsh advfirewall firewall show rule name=`"$name`""
    Write-Host $check

    if(-Not($check -like "Rule*")) 
    {
        Write-Host "Adding firewall rule `"$name`" to open port $port"
        iex "netsh advfirewall firewall add rule name=`"$name`" dir=in action=allow protocol=TCP localport=$port"
    }
}


function InstallElasticSearch
{
    Unpack "https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/zip/elasticsearch/2.4.0/elasticsearch-2.4.0.zip" "elasticsearch-2.4.0.zip" "elasticsearch-2.4.0"

    AddFirewallRule "open port 9200" 9200

    $elasticSearchInstallScript = "$PSScriptRoot\..\elasticsearch-2.4.0\elasticsearch-2.4.0\bin\service.bat"
    
    iex 'net stop elasticsearch-service-x64'
    iex "$elasticSearchInstallScript install"
    iex 'net start elasticsearch-service-x64'

    #check es 
    Test-port 127.0.0.1 9200 20

    #disable date detection:
    $disable_date_detection = "$curl -X PUT -d @$PSScriptRoot\disable_es_kibana_index_date_detection.json http://127.0.0.1:9200/_template/disable_date_detection"
    Write-Host "Disabling date detection in Elastic Search for logstash* indices: $disable_date_detection"
    iex $disable_date_detection

    #install head plugin:
    iex "cmd.exe /C `"$PSScriptRoot\..\elasticsearch-2.4.0\elasticsearch-2.4.0\bin\plugin install mobz/elasticsearch-head`""

}

function InstallLogstash
{
    Unpack "https://download.elastic.co/logstash/logstash/logstash-2.4.0.zip" "logstash-2.4.0.zip" "logstash-2.4.0"

    AddFirewallRule "open port 5044" 5044

    #copy start script for service
    If (-Not (Test-Path "$PSScriptRoot\..\logstash-2.4.0\logstash-2.4.0\bin\runlogstash.bat")){
        Copy-Item "$PSScriptRoot\runlogstash.bat" "$PSScriptRoot\..\logstash-2.4.0\logstash-2.4.0\bin"
    }
    #copy logstash config to bin
    Copy-Item "$PSScriptRoot\logstash.conf" "$PSScriptRoot\..\logstash-2.4.0\logstash-2.4.0\bin" -force
        
    iex "net stop logstash"
    Install-Service "logstash" "$PSScriptRoot\..\logstash-2.4.0\logstash-2.4.0\bin\runlogstash.bat"    
    iex 'net start logstash'

    #check logstash port
    Test-port 127.0.0.1 5044 10
}

function InstallKibana
{
    Unpack "https://download.elastic.co/kibana/kibana/kibana-4.6.1-windows-x86.zip" "kibana-4.6.1-windows-x86.zip" "kibana-4.6.1"

    AddFirewallRule "open port 5601" 5601

    iex 'net stop kibana'
    Install-Service "kibana" "$PSScriptRoot\..\kibana-4.6.1\kibana-4.6.1-windows-x86\bin\kibana.bat"
    iex 'net start kibana'

    Test-port 127.0.0.1 5601
    #iex "$curl -X PUT -d @$PSScriptRoot\checkoutDashboard.json http://localhost:9200/kibana-int/dashboard/checkout"
}

function InstallFilebeat
{
    Unpack "https://download.elastic.co/beats/filebeat/filebeat-1.3.1-windows.zip" "filebeat-1.3.1-windows.zip" "filebeat-1.3.1"
    Copy-Item "$PSScriptRoot\filebeat.yml" "$PSScriptRoot\..\filebeat-1.3.1\filebeat-1.3.1-windows" -force
    $addIndex = "$curl -X PUT -d @$PSScriptRoot\filebeat.template.json http://127.0.0.1:9200/_template/filebeat?pretty"
    
    Write-Host "Adding index template to Elastic Search: $addIndex"
    iex $addIndex
    iex "$PSScriptRoot\..\filebeat-1.3.1\filebeat-1.3.1-windows\install-service-filebeat.ps1"
    iex 'net start filebeat'
}

#nssm
Unpack "https://nssm.cc/release/nssm-2.24.zip" "nssm-2.24.zip" "nssm"

#curl
Unpack "https://dl.uxnr.de/build/curl/curl_winssl_msys2_mingw64_stc/curl-7.51.0/curl-7.51.0.zip" "curl-7.51.0.zip" "curl-7.51.0"
$curl = "$PSScriptRoot\..\curl-7.51.0\src\curl.exe"

# UninstallAll
 InstallElasticSearch
 InstallLogstash
 InstallKibana
 InstallFilebeat



