. .\nssmInstallService.ps1

function StopAll
{
    iex 'net stop elasticsearch-service-x64'
    iex 'net stop logstash'
    iex 'net stop kibana'
    iex 'net stop filebeat'
}

function UninstallAll
{
    StopAll

    Uninstall-Service elasticsearch-service-x64
    Uninstall-Service logstash
    Uninstall-Service kibana
    Uninstall-Service filebeat
}

UninstallAll
