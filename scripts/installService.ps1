function Uninstall-Service
{
     param([string]$serviceName)
     # delete service if it already exists
    if (Get-Service $serviceName -ErrorAction SilentlyContinue) {

        Write-Host "DELETING SERVICE $serviceName"

        $service = Get-WmiObject -Class Win32_Service -Filter "name='$serviceName'"
        $service.StopService()
        Start-Sleep -s 3
        $service.delete()
    }
}

function Install-Service
{
    param([string]$serviceName, [string]$path)

    Uninstall-Service $serviceName

    Write-Host "CREATING SERVICE $serviceName AT $path"

    # create new service
    New-Service -name $serviceName `
      -displayName $serviceName `
      -binaryPathName $path

}




